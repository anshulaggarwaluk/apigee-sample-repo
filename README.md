# Apigee Sample Repository #
## Points to note
- All API proxies to go into the /src/gateway folder
- All Apigee shared-flows to go into the /src/sharedFlows folder
- All platform confic to go into the /src/edgeConfig/edge folder. This folder has sepratae folders for API level, Environment Level and Org Level configurations.
- All API specs to be published on Dev Portal must go to /src/edgeConfig/specs folder.
- The gateway folder has a shared-pom.xml which is the parent pom of all API proxies. Each API proxy to have its own pom.xml. Refer to the sample API proxy /src/gateway/Mock-v1
- The sharedflows folder has a parent-sharedflow-pom.xml which is the parent pom of all shared-flows. Each shared-flow to have its own pom.xml. Refer to the sample API proxy /src/sharedflows/security
- All tests within a proxy must go to the tests folder within the proxy folder.
- All resources used by the proxy must go to the resources folder within the API proxy folder. This would follow the same structure as the one which you get when export the proxy bundle.

## Deploy Proxy/SharedFlow
- [Sign up for an Apigee Account!](https://accounts.apigee.com/accounts/sign_up?callback=https://enterprise.apigee.co). Not required if already provided.
- [Install Maven 3 from Software Catalog.*](https://software.uk.deloitte.com/)
- Clone this repo 'git clone https://AnshulaggarwalUK@bitbucket.org/anshulaggarwaluk/apigee-sample-repo.git'
- For API Proxy - ```cd src/gateway/Mock-v1``` *Recommended template*
- For sharedflow - ```cd src/sharedflows/security``` *Recommended template*
- Execute ```mvn install -Ptest -Dusername={apigee-edge-email} -Dpassword={apigee-edge-password} -Dorg={apigee-edge-org}```

That's it! If everything ran smooth, you will see BUILD SUCCESS message at the of the execution of this command. Next steps, learn a bit of Maven to customize the pom.xml.

## Basic Commands – apigee.options

### Configure, package, import, deploy, and test bundle (default validate apigee.option) – Creates new revision

```mvn install -Ptest -Dusername=$ae_username -Dpassword=$ae_password -Dorg=testmyapi```

### Configure, package, import, override, deploy, and test bundle (default validate apigee.option) – Overrides current revision

```mvn install -Ptest -Dusername=$ae_username -Dpassword=$ae_password -Doptions=validate,update```

### Undeploy and delete revision in the environment

```mvn install -Ptest -Dusername=$ae_username -Dpassword=$ae_password -Doptions=clean```

### Configure and package bundle. Does not import

```mvn package -Ptest -Dusername=$ae_username -Dpassword=$ae_password -Dorg=testmyapi```

### The following are available options:
a. clean - This will delete the last deployed revision in an environment.

b. validate - This will validate a bundle before importing. Thus if you want strict validation then its required.

c. inactive - This will just import the bundle without activating the bundle.

d. override - This is used for seamless deployment. This must be supplied with apigee.override.delay parameter. The apigee.override.delay expects delay to be given in seconds.

e. update - It will update the deployed revision .  This is similar to import with validation but no new revision is created. If there any errors in the bundle, error is thrown and the existing bundle is left intact. In case the revision they are trying to update is deployed, it will internally trigger undeployment and deployment. It is completely in the background and not visible in the response. It is advised not to update the deployed revision. (UI could show a warning or something in this case).


## EdgeConfig
```
/src/edgeConfig
```

This project demonstrates the creation and management of Apigee Edge Config and performs the following steps in sequence.
  - Creates Caches
  - Creates Target servers
  - Creates Virtual Hosts (needs truststores and keyRef's setup using ManagementUI)
  - Creates API products
  - Creates Developers
  - Creates Developer Apps
  - Creates Companies

To use, edit src/edgeConfig/shared-pom.xml, and update org and env elements in all profiles to point to your Apigee org, env. You can add more profiles corresponding to each env in your org.

      <apigee.org>myorg</apigee.org>
      <apigee.env>test</apigee.env>

To run the plugin and use edge.json jump to samples project `cd /src/edgeConfig` and run 

`mvn install -Ptest -Dusername=<your-apigee-username> -Dpassword=<your-apigee-password> -Dapigee.config.options=create`

To run the plugin and use a config file similar to edge.json in any directory jump to samples project `cd /samples/EdgeConfig` and run 

`mvn install -Ptest -Dusername=<your-apigee-username> -Dpassword=<your-apigee-password> -Dapigee.config.file=<path-to-config-file> -Dapigee.config.options=create`

To run the plugin and use the multi-file format jump to samples project `cd /samples/EdgeConfig` and run 

`mvn install -Ptest -Dusername=<your-apigee-username> -Dpassword=<your-apigee-password> -Dapigee.config.options=create -Dapigee.config.dir=resources/edge`